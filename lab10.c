#include<stdio.h>

void input(int *a, int *b)
{
	printf("Enter first number:\n");
	scanf("%d",a);
	printf("Enter second number:\n");
	scanf("%d",b);
}
int addition(int *a, int *b)
{
	return *a+*b;
}
int subraction(int *a, int *b)
{
	return *a-*b;
}
int multiplication(int *a, int *b)
{
	return (*a)*(*b);
}
int division(int *a, int *b)
{
	return (*a)/(*b);
}
int Remainder(int *a, int *b)
{
	int r;
	r=(*a)%(*b);
	return r;
}
void output(int *a, int *b ,int sum, int diff,int mult, int div, int rem)
{
	printf("Sum of %d and %d is %d\n",*a,*b,sum);
    printf("Difference of %d and %d is %d\n",*a,*b,diff);
    printf("Product of %d and %d is %d\n",*a,*b,mult);
    printf("Quotient of %d and %d is %d\n",*a,*b,div);
    printf("Remainder of %d and %d is %d\n",*a,*b,rem);
}
int main()
{
	int a,b;
	int sum,diff,mult,div,rem;
	input(&a,&b);
	sum=addition(&a,&b);
	diff=subraction(&a,&b);
    mult=multiplication(&a,&b);
    div=division(&a,&b);
    rem=Remainder(&a,&b);
    output(&a,&b,sum,diff,mult,div,rem);
    return 0;
}