#include <stdio.h> 
#include <stdlib.h> 
float range(float a)
{
    if(a>=1 && a<=1000) 
    	{ 		 	
    	    return 0; 	
    	} 
    	else 	
    { 	
        return 1;
    } 
}
int main() 
{ 	
    FILE* fptr; 	
    float a; 
    int res;
    	fptr=fopen("RANGE.txt","w"); 	
    if(fptr==NULL) 	
    { 		
        printf("ERROR!!\n"); 
       		exit(1); 
    	} 	
    // set the range as 1 to 1000 
    	printf("Enter a number between 1 and 1000:\n"); 	
    scanf("%f",&a); 	
    res = range(a);
    if(res == 0)
    {
        fprintf(fptr,"%f",a);
    }
    if(res == 1)
    {
        printf("Given number is Not in Range\n"); 	
        return 0;
    }
    	fclose(fptr); 
    	fptr=fopen("RANGE.txt","r"); 
    	if(fptr==NULL) 	
    { 		
        printf("ERROR!!\n"); 	
        	exit(1); 	
    } 	
    fscanf(fptr,"%f", &a); 
    printf("Value of number in file =%.2f", a); 
    fclose(fptr); 	
    return 0; 
    }